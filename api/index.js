var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mysql = require('mysql');
var expressJWT = require('express-jwt');
var jwt = require('jsonwebtoken');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(expressJWT({secret:"myscreat"}).unless({path: ['/api/user']}));
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'reactblogpost'
});

connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,AUTHORIZATION");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
});

app.get('/api/posts/count', function (req, res) {
    connection.query('SELECT * FROM posts', function (error, results, fields) {
        if (error) throw error;
        res.json(results);
    });

});

app.get('/api/posts/page/:page_id', function (req, res) {
    var page_id = req.params.page_id;
    var limit = (page_id * 2) - 2 ;
    connection.query('SELECT * FROM posts LIMIT '+limit+',2', function (error, results, fields) {
        if (error) throw error;
        res.send(results);
    });

});

app.post('/api/posts', function (req, res) {
    var title = req.body.title;
    var categories = req.body.categories;
    var content = req.body.content;
    var post  = {title: title,categories:categories,content:content};
    var query = connection.query('INSERT INTO posts SET ?', post, function (error, results, fields) {
        if (error) throw error;
        res.send("Success");
    });

});

app.get('/api/posts/:id', function (req, res) {
    var page_id = req.params.id;
    var query = connection.query('SELECT * FROM posts WHERE id = ?', page_id, function (error, results, fields) {
        if (error) throw error;
        res.json(results[0]);
    });

});

app.delete('/api/posts/:id',function(req,res){
    var page_id = req.params.id;
    var query = connection.query('DELETE FROM posts WHERE id = ?', page_id, function (error, results, fields) {
        if (error) throw error;
        res.send("Success");
    });
});

app.post('/api/user',function(req,res){
    var email = req.body.email;
    var password = req.body.password;

    var token = jwt.sign({email:email,password:password},"myscreat");
    var query = connection.query('SELECT * FROM users WHERE email=? and password=?',[email,password],function(error,results,fields){
        if(error) throw error;
        if(results.length > 0){
            res.send(token);
        }
        else{
            res.send();
        }
    })
});

app.listen(8000);