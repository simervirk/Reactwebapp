import React,{ Component } from "react";
import { Link } from "react-router";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { logMeOut } from "../actions/index";
import { browserHistory } from 'react-router';

class Header extends Component{

    constructor(props){
        super(props);
        this.logout = this.logout.bind(this);
    }

    logout(event){
        event.preventDefault();
        this.props.logMeOut();
    }

    render(){
        if(this.props.session){
            var loggedInLink = <a href='/logout' onClick={this.logout}>Logout</a>;
        }
        else{
            var loggedInLink = <a href='/login'>Login</a>;
            browserHistory.push("/login");
        }
        return (
            <div>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="#">React Blog</a>
                        </div>
                        <ul className="nav navbar-nav navbar-right">
                            <li className="active"><Link to="/">Home</Link></li>
                            <li><Link to="/posts/new">Add Post</Link></li>
                            <li>{loggedInLink}</li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        session:state.session.session
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({logMeOut},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Header);
