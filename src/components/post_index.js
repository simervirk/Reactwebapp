import React,{ Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPosts } from '../actions/index';
import { setPage } from '../actions/index';
import { fetchTotalPosts } from '../actions/index';
import { Link } from 'react-router';
import Header  from './header';
import _ from 'lodash';

class postIndex extends Component{

    componentWillMount(){
        this.props.fetchTotalPosts();
        this.props.fetchPosts(this.props.currentPage);
    }

    renderPosts() {
        if(!this.props.posts || this.props.posts == ""){
            return <div>Loading...</div>
        }
        const posts = this.props.totalPosts;
        return this.props.posts.map((post) => {
            return (
                <li className="list-group-item" key={post.id}>
                    <Link to={"posts/"+post.id}>
                        <span className="pull-xs-right">{post.categories}</span>
                        <strong>{post.title}</strong>
                    </Link>
                </li>
            )
        })
    }

    navigatePage(page_no){
        this.props.setPage(page_no);
        this.props.fetchPosts(page_no);
    }

    renderPagination(){
        const posts = this.props.totalPosts;
        if(posts.length < 2){
            return null;
        }
        const current_page = this.props.currentPage;
        const offset = 2;
        const range = [];
        for (let i = 1; i <= _.round(posts.length/offset); ++i) {
            range.push(i);
        }
        return(
            <ul className="pagination">
                <li onClick={this.navigatePage.bind(this,1)}>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                {
                    range.map(v=> {
                        const isCurrent = v === current_page;
                        return (
                            <li onClick={this.navigatePage.bind(this,v)} className={isCurrent ? 'page-item active' : 'page-item'} key={v}>
                                <a href="#">{v}</a>
                            </li>
                        )
                    })
                }

                <li onClick={this.navigatePage.bind(this,(range.length))}>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        )
    }

    render(){

        return(
            <div>
                <Header></Header>
                <h3>Posts</h3>
                <ul className="list-group">
                    {this.renderPosts()}
                </ul>
                <nav aria-label="Page navigation">
                    {this.renderPagination()}
                </nav>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchPosts,setPage,fetchTotalPosts},dispatch);
}

function mapStateToProps(state){
    return {
        posts:state.posts.all,
        currentPage:state.posts.currentPage,
        totalPosts:state.posts.totalPosts
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(postIndex);