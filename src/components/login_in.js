import React,{ Component,PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { loggedInUser } from '../actions/index';
import { browserHistory } from 'react-router';

class loginIn extends Component{

    onSubmit(props){
        this.props.loggedInUser(props);
    }

    render(){
        if(this.props.session){
            browserHistory.push("/");
        }
        const { fields:{email,password},handleSubmit } = this.props;
        return(
            <div className="wrapper">
                <form className="form-signin" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <h2 className="form-signin-heading">Please login</h2>
                    <div className={`form-group ${email.touched && email.invalid ? 'has-danger' : ''}`}>
                        <input type="text" className="form-control" placeholder="Email Address" {...email}/>
                    </div>
                    <div className={`form-group ${password.touched && password.invalid ? 'has-danger' : ''}`}>
                        <input type="password" className="form-control" placeholder="Password" {...password}/>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                    </div>
                    <div className="text-help text-danger">
                        {email.touched ? email.error : ""}
                    </div>
                    <div className="text-help text-danger">
                        {password.touched ? password.error : ""}
                    </div>
                </form>
            </div>
        );
    }
}
function validate(values){
    const errors = {};
    if(!values.email){
        errors.email = "Please enter Email Address";
    }
    if(!values.password){
        errors.password = "Please enter Password";
    }
    return errors;
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({loggedInUser},dispatch);
}

function mapStateToProps(state){
    return {
        session:state.session.session
    }
}

export default reduxForm({
    form:"loginForm",
    fields:['email','password'],
    validate
},mapStateToProps,mapDispatchToProps)(loginIn);