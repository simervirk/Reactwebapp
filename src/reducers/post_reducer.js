import { FETCH_POSTS } from "../actions/index";
import { SHOW_POST } from "../actions/index";
import { SET_PAGE } from "../actions/index";
import { TOTAL_POSTS } from "../actions/index";
import { LOG_IN_SUCCESS } from "../actions/index";
import INITIAL_STATE from "./intialState";

export default function(state = INITIAL_STATE,action){
    switch(action.type){
        default:
            return state;

        case FETCH_POSTS:
            return {...state,all:action.payload.data };

        case SHOW_POST:{
            return {...state,post:action.payload.data };
        }
        case SET_PAGE:{
            return {...state,currentPage:action.payload};
        }
        case TOTAL_POSTS:{
            return {...state,totalPosts:action.payload.data};
        }
    }
}