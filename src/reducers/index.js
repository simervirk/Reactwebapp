import { combineReducers } from 'redux';
import  postReducer from './post_reducer';
import sessionReducer from './session_reducer';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  posts:postReducer,
  form: formReducer,
  session:sessionReducer
});

export default rootReducer;
