import { LOG_IN_SUCCESS } from "../actions/index";
import INITIAL_STATE from "./intialState";
import { LOG_OUT } from "../actions/index";

export default function (state=INITIAL_STATE,action){
    switch (action.type){
        default:
            return state;
        case LOG_IN_SUCCESS:
            return {...state,session:true};
        case LOG_OUT:
            return {...state,session:false};
    }
}