import axios from 'axios';

//const ROOT_URL ="http://reduxblog.herokuapp.com/api";
const ROOT_URL ="http://www.localhost:8000/api";
const API_KEY = "?key=simervirkssjfnnnakd";

export const FETCH_POSTS = "FETCH_POSTS";
export const CREATE_POSTS = "CREATE_POSTS";
export const SHOW_POST = "SHOW_POST";
export const DELETE_POST = "DELETE_POST";
export const SET_PAGE = "SET_PAGE";
export const TOTAL_POSTS = "TOTAL_POSTS";
export const LOG_IN_SUCCESS = "LOG_IN_SUCCESS";
export const LOG_OUT = "LOG_OUT";

export function fetchPosts(page_id){
    const request = axios.get(`${ROOT_URL}/posts/page/${page_id}`,{headers: {'AUTHORIZATION': `Bearer ${sessionStorage.jwt}`}});
    return {
        type:FETCH_POSTS,
        payload: request
    }
}

export function createPost(props){
    const request = axios.post(`${ROOT_URL}/posts/`,props,{headers: {'AUTHORIZATION': `Bearer ${sessionStorage.jwt}`}});
    return {
        type:CREATE_POSTS,
        payload: request
    }
}

export function showPost(post_id){
    const request = axios.get(`${ROOT_URL}/posts/${post_id}`,{headers: {'AUTHORIZATION': `Bearer ${sessionStorage.jwt}`}});
    return {
        type:SHOW_POST,
        payload: request
    }
}

export function deletePost(post_id){
    const request = axios.delete(`${ROOT_URL}/posts/${post_id}`,{headers: {'AUTHORIZATION': `Bearer ${sessionStorage.jwt}`}});
    return {
        type:DELETE_POST,
        payload:request
    }
}

export function setPage(page_no){
    //const request = axios.delete(`${ROOT_URL}/posts/${post_id}${API_KEY}`);
    return {
        type:SET_PAGE,
        payload:page_no
    }
}

export function fetchTotalPosts(){
    const request = axios.get(`${ROOT_URL}/posts/count`,{headers: {'AUTHORIZATION': `Bearer ${sessionStorage.jwt}`}});
    return {
        type:TOTAL_POSTS,
        payload: request
    }
}

function loginSuccess() {
    return {
        type:LOG_IN_SUCCESS
    }
}

export function loggedInUser(credentials){
    const request = axios.post(`${ROOT_URL}/user`,credentials);
    return (dispatch)=>{
      request.then(({data}) => {
          if(data){
              sessionStorage.setItem('jwt', data);
              dispatch({type:LOG_IN_SUCCESS});
          }
      })
    };
}

export function logMeOut(){
    sessionStorage.removeItem('jwt');
    return {
        type:LOG_OUT
    }
}