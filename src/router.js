import React,{ Component } from 'react';
import Route,{IndexRoute} from 'react-router';

import App from './components/app';
import postIndex from './components/post_index';
import postNew from './components/post_new';
import postShow from './components/post_show';
import loginIn from './components/login_in';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={postIndex}/>
      <Route path="login" component={loginIn} />
      <Route path="posts/new" component={postNew} onEnter={requireAuth}/>
      <Route path="posts/:id" component={postShow} onEnter={requireAuth}/>
  </Route>
);

function requireAuth(nextState, replace){
  if(!sessionStorage.jwt){
    replace({
      pathname: "/login",
      state: {nextPathname:nextState.location.pathname}
    })
  }
}